package net.dorskiy.test;

import java.io.IOException;
import java.util.Date;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.SmackAndroid;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.ConnectionException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	private final String LOG_TAG = "myLogs";
	private LocationManager locationManager;
	private TextView myCoord;
	private Button btnConnect, btnDisconnect;
	private EditText edHost,edJid,edPass; 
	private String host,jid,password;
	private XMPPConnection connection;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		myCoord = (TextView)findViewById(R.id.tvCoord);
		edHost = (EditText)findViewById(R.id.etHost);
		edJid = (EditText)findViewById(R.id.etJid);
		edPass = (EditText)findViewById(R.id.etPassword);
		btnConnect = (Button)findViewById(R.id.btnConnect);
		btnDisconnect = (Button)findViewById(R.id.btnDisconnect);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    if (resultCode == 1) {
	       switch (requestCode) {
	          case 1:
	           break;
	        }
	     }  
	 }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void connect(View view) {	
	    if(edHost.getText().toString().equals("") || edJid.getText().toString().equals("") || edPass.getText().toString().equals("")){
	    	Toast.makeText(getApplicationContext(), "Fill all fields", Toast.LENGTH_SHORT).show();
	    	return;
	    }
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
		        1000 * 10, 10, locationListener);
	    host = edHost.getText().toString();
	    jid = edJid.getText().toString();
	    password = edPass.getText().toString();
	    ConnectAsync connectAsync = new ConnectAsync();
	    connectAsync.execute(host,jid,password);
	}
	
	public void disconnect(View view){
			setEnable(true);
			DisconnectAsync disconnectAsync = new DisconnectAsync();
			disconnectAsync.execute(jid);
	}
	
	class DisconnectAsync extends AsyncTask<String, Void, Void>{
		protected Void doInBackground(String... params) {
			try {
				Message message = new Message();
				message.setBody(params[0] + " disconnected from server");
				connection.sendPacket(message);
				connection.disconnect();
			} catch (NotConnectedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		};
		
		@Override
		protected void onPostExecute(Void result) {
			if(!connection.isConnected())
				Toast.makeText(getApplicationContext(), "Disconnected from server", Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(getApplicationContext(), "Disconnection error", Toast.LENGTH_SHORT).show();
			super.onPostExecute(result);
		}
	};
	
	class SendAsync extends AsyncTask<String, Void, Void>{
		protected Void doInBackground(String[] params) {
			Message message = new Message();
			message.setBody(params[0]);
			try {
				connection.sendPacket(message);
			} catch (NotConnectedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		};
		@Override
		protected void onPostExecute(Void result) {
			Toast.makeText(getApplicationContext(), "Sent your location", Toast.LENGTH_SHORT).show();
			super.onPostExecute(result);
		}
	};
	
	class ConnectAsync extends AsyncTask<String, Void, Void>{	
		@Override
		protected Void doInBackground(String... params) {
			Context context = getApplicationContext();
            SmackAndroid.init(context);
            ConnectionConfiguration ConnectionConfiguration = new ConnectionConfiguration(params[0], 5222);
            ConnectionConfiguration.setDebuggerEnabled(true);
            ConnectionConfiguration.setSecurityMode(SecurityMode.disabled);
            connection = new XMPPTCPConnection(ConnectionConfiguration);   
            try {
                connection.connect();
                connection.login(params[1], params[2]);
                Message message = new Message();
                message.setBody(params[1] + " connected to server");
                connection.sendPacket(message);
            } catch (ConnectionException e) {
            	Log.i(LOG_TAG,e.getFailedAddresses().get(0).getErrorMessage()+"");
                e.printStackTrace();
            } catch (SmackException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XMPPException e) {
                e.printStackTrace();
            }
            return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			if(connection.isConnected()){
				setEnable(false);
				Toast.makeText(getApplicationContext(), "Connected to server", Toast.LENGTH_SHORT).show();
			}
			else{
			    setEnable(true);
			    Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_SHORT).show();
			}
			super.onPostExecute(result);
		}
	};
	
	private void setEnable(boolean value){
		btnConnect.setEnabled(value);
		edHost.setEnabled(value);
		edJid.setEnabled(value);
		edPass.setEnabled(value);
		btnDisconnect.setEnabled(!value);
	}
	
	private LocationListener locationListener = new LocationListener() {

		@Override
		public void onLocationChanged(Location location) {
			showLocation(location);
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onProviderEnabled(String provider) {
			showLocation(locationManager.getLastKnownLocation(provider));
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};
	
	private void showLocation(Location location) {
		if (location == null)
			return;
		if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
			myCoord.setVisibility(View.VISIBLE);
			myCoord.setText(formatLocation(location));
			SendAsync sendAsync = new SendAsync();
		    sendAsync.execute(myCoord.getText().toString());
		}
	}

	private String formatLocation(Location location) {
	    if (location == null)
	      return "";
	    return String.format(
	        " lat = %f \n lon = %f \n time = %3$tF %3$tT",
	        location.getLatitude(), location.getLongitude(), new Date(location.getTime()));
	  }

}
